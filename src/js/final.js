/*
*  微信扫码出纸巾完成页面
* */
'use strict';

var Final = {
    code: '',
    /*
    * 初始化页面
    * */
    init: function () {
        Common.addLoadingPage();
        //绑定网页回退事件
        Common.bindBackEvent();
        //调用出纸接口
        this.completeTask();
    },
    /*
    * 纸巾领取完成
    * */
    completeTask: function() {
        var context = this,
            code = Common.getUrlParam('code'),
            qid = Common.getUrlParam('qid'),
            url = "/service/wx/completeTask";

        if(!code || !qid) {
            Common.hideLoadingPage();
            context.showErrorPage();
            return;
        }

        context.code = code;

        $.ajax({
            type: "GET",
            url: url,
            data: {
                code: code,
                advCode: qid
            },
            dataType: "json",
            success: function(res) {
                Common.hideLoadingPage();
                if(!res || res.status != 0) {
                    if(res.status == -2) {
                        //免费纸巾领取额度用完
                        context.showNoTimesPage();
                    } else if(res.status == -3) {
                        //code失效
                        // Common.toast("链接已失效，请重新扫码！");
                        //显示成功页面
                        Common.showTissueSuccessPage();
                    } else {
                        //出纸失败
                        context.showErrorPage();
                    }
                } else {
                    Common.setOpenId(res.data.openId);
                    //显示成功页面
                    Common.showTissueSuccessPage();
                }
            },
            error: function () {
                Common.hideLoadingPage();
                //显示错误页面
                context.showErrorPage();
            }
        });
    },
    /*
    * 显示错误页面
    * */
    showErrorPage: function() {
        $("body div").remove();

        var html = '<div class="result-error">' +
            '    <div class="re-mask"></div>' +
            '    <div class="re-inner">' +
            '        <div class="re-text get-error"></div>' +
            '        <div class="again-btn" id="btnAgain" onclick="Final.completeTask()"></div>' +
            '    </div>' +
            '</div>';

        $(document.body).append($(html));
    },
    //显示免费额度已用完页面
    showNoTimesPage: function() {
        $("body div").remove();

        var html = '<div class="page_index">' +
            '    <div class="no-tissue">' +
            '        <div class="no-tissue-text"></div>' +
            '        <div class="btn-buy" id="btnBuy" onclick="Common.onBuyTissueClick()"></div>' +
            '    </div>' +
            '</div>';

        $(document.body).append($(html));
    }
}

Final.init();