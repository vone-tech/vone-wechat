/*
* 扫码首页
* */
var Index = {
    sid: null,
    posArray: [],
    posCheckTimes: 0,
    geolocation: null,
    init: function () {
        Common.clearInfo();
        Common.addLoadingPage();

        this.sid = Common.getUrlParam('sid');

        if (!this.sid) {
            Common.hideLoadingPage();
            Common.showErrorPage("未找到设备ID, 请退出重试或联系客服");
            return;
        }

        this.initLocation();
    },
    //初始化地图
    initLocation: function () {
        this.geolocation = new BMap.Geolocation();
        //获取三次位置，去除漂移的点
        this.getLocation();
    },
    //获取定位
    getLocation: function() {
        var context = this;

        this.geolocation.getCurrentPosition(function(r){
            if(this.getStatus() == BMAP_STATUS_SUCCESS){
                // alert('您的位置：'+r.point.lng+','+r.point.lat);
                //获取三个点位后开始计算
                if(context.posCheckTimes >= 3) {
                    context.checkLocation();
                } else {
                    context.posArray.push({
                        longitude: r.point.lng,
                        latitude: r.point.lat,
                        index: context.posCheckTimes
                    });

                    context.posCheckTimes ++;

                    context.getLocation();
                }
            } else {
                Common.showErrorPage("获取定位失败, 请退出重试或联系客服");
            }
        },{enableHighAccuracy: true});
    },
    //检查定位是否在机器所在范围内
    checkLocation: function() {
        var context = this,
            // ajaxUrl = "http://192.168.0.104:18080/service/wx/checkLocation";
            ajaxUrl = "/service/wx/checkLocation";
        //
        // for (var i = 0; i < this.posArray.length; i++) {
        //     console.log('您的位置：'+this.posArray[i].longitude+','+this.posArray[i].latitude);
        // }

        if(this.posArray.length < 3) {
            Common.showErrorPage("获取定位失败, 请退出重试或联系客服");
            return;
        }

        $.ajax({
            type: "GET",
            url: ajaxUrl,
            data: {
                sid: this.sid,
                longitude: this.posArray[0].longitude,
                latitude: this.posArray[0].latitude,
                longitude1: this.posArray[1].longitude,
                latitude1: this.posArray[1].latitude,
                longitude2: this.posArray[2].longitude,
                latitude2: this.posArray[2].latitude
            },
            dataType: "json",
            success: function (res) {
                Common.hideLoadingPage();

                // alert(JSON.stringify(res.data));

                if(!res || res.status != 0) {
                    Common.showErrorPage("请在机器所在的位置扫码");
                    return;
                }

                context.goToWeixinPage();
            },
            error: function () {
                Common.hideLoadingPage();
                //显示错误页面
                Common.showErrorPage();
            }
        });
    },
    //跳转到微信扫纸页面
    goToWeixinPage: function () {
        var redirect_uri = encodeURIComponent('https://vonechn.com/weixin/home.html?sid=' + this.sid);

        window.location.href="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx9d8d2cb6c5fcd0fe&redirect_uri="+ redirect_uri +"&response_type=code&scope=snsapi_base&state=vone#wechat_redirect";
    }
}

Index.init();