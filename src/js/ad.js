/*
*  微信扫码出纸巾
* */
'use strict';

var Ad = {
    $loadSuccessElem: null, //加载成功元素
    $hasAdElem: null, //有广告元素
    $name: null, //标题
    $title: null, //二级标题
    $qrcodeImg: null, //二维码
    $qrcodeImgError: null, //二维码错误元素
    $adSliderWrap: null, //广告幻灯片容器
    adIndex: 0, //广告列表索引,
    sid: null, //设备id
    //广告列表
    adList: [
        // {
        //     bgImg: '/image/silder_bg.jpg',
        //     logoImg: '/image/tmp_logo.png',
        //     qrCodeImg: '/image/tmp_qrcode.png'
        // }
    ],
    /*
    * 初始化页面
    * */
    init: function () {
        this.$loadSuccessElem = $('.ad');
        this.$hasAdElem = $('.app-ad');
        
        this.$adSliderWrap = $('.swiper-wrapper');

        this.$name = $('.title');
        this.$title = $('.sub-title');
        this.$qrcodeImg = $('.qrcode-img');
        this.$qrcodeImgError = $('.qrcode-img-error');

        this.getAdList();
    },
    /*
    * 获取广告列表
    * */
    getAdList: function() {
        var context = this,
            code = Common.getUrlParam('code'),
            url = "/service/wx/getDisplayQrcode";

        if(!code) {
            Common.hideLoadingPage();
            Common.showErrorPage("无法获取用户ID, 请退出重试或联系客服");
            return;
        }

        $.ajax({
            type: "GET",
            url: url,
            data: {
                code: code
            },
            dataType: "json",
            success: function(res) {
                Common.hideLoadingPage();

                if(!res || !res.data) {
                    Common.showErrorPage("暂无广告, 请退出重试或联系客服");
                    return;
                }

                if(res.data.length <= 0) {
                    Common.showErrorPage("暂无广告, 请退出重试或联系客服");
                    return;
                }

                context.adList = res.data;

                context.initAdContent().bindEvents();
            },
            error: function () {
                Common.hideLoadingPage();
                //显示错误页面
                Common.showErrorPage();
            }
        });
    },
    /*
    * 初始化广告内容
    * */
    initAdContent: function() {
        this.$loadSuccessElem.removeClass(CLS_HIDE);

        this.initSlider(this.adList);

        return this;
    },
    /*
    * 初始化幻灯片
    * */
    initSlider: function(adList) {
        for(var i=0; i<adList.length; i++) {
            this.$adSliderWrap.append(this.genSliderElem(adList[i]));
        }

        new Swiper('.swiper-container',{
            loop:true,
            grabCursor: false
        });
    },
    genSliderElem: function(ad) {
        var html = '<div class="swiper-slide">' +
            '                <div class="silder_out" style="background-image: url('+ ad.bgImg +')">' +
            '                    <img src="'+ ad.logoImg +'" class="ad-logo">' +
            '                    <img src="'+ ad.qrCodeImg +'" class="ad-qrcode">' +
            '                    <img src="image/2-2.png" class="goin-btn">' +
            // '                    <img src="image/2-3.png" class="get-btn">' +
            '                </div>' +
            '            </div>';

        return html;
    },
    /*
    * 绑定事件
    * */
    bindEvents: function () {
        var context = this;

        // this.$qrcodeImg.load(function(){
        //     context.$qrcodeImgError.addClass(CLS_HIDE);
        //     $(this).addClass(CLS_LOADED);
        // });
        //
        // this.$qrcodeImg.error(function(){
        //     $(this).addClass(CLS_HIDE);
        //     context.$qrcodeImgError.removeClass(CLS_HIDE);
        // });
    }
}

Ad.init();