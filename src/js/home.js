/*
* 首页
* */
var Home = {
    sid: '', //设备id
    init: function () {
        //绑定网页回退事件
        Common.bindBackEvent();
        //获取设备id
        this.getDeviceId();
        // for test
        // this.showSuccessPage();
    },
    //购买点击事件
    onBuyTissue: function() {
        var data = {
            type: 'new',
            sid: this.sid,
            openid: Common.getOpenId(),
            url: encodeURIComponent(location.href.split("#")[0])
        }

        Common.onBuyTissue(data);
    },
    /*
    * 获取纸巾机的ID
    * */
    getDeviceId: function () {
        Common.addLoadingPage();

        this.sid = Common.getUrlParam('sid');
        if (!this.sid) {
            Common.hideLoadingPage();
            Common.showErrorPage("未找到设备ID, 请退出重试或联系客服");
            return;
        }

        var code = Common.getUrlParam('code');
        if (!code) {
            Common.hideLoadingPage();
            Common.showErrorPage("用户授权失败, 请退出重试或联系客服");
            return;
        }

        this.getQrcodeUrl(this.sid, code);
    },
    /*
    * 获取广告列表
    * */
    getQrcodeUrl: function (sid, code) {
        var context = this,
            ajaxUrl = "/service/wx/createBoxQrcodeNew";

        $.ajax({
            type: "GET",
            url: ajaxUrl,
            data: {
                sid: sid,
                code: code
            },
            dataType: "json",
            success: function (res) {
                Common.hideLoadingPage();

                // alert(JSON.stringify(res.data));

                if(!res || !res.data) {
                    Common.showErrorPage("服务器不太给力, 请退出重试或联系客服");
                    return;
                }

                var box = res.data.box;
                //机器状态:R-运行中，E-异常，U-未知状态, S-停机
                if(!box || (box.tissueCount <= MAX_TISSUE_COUNT) || (box.status != "R")) {
                    Common.showMachineErrorPage();
                    return;
                }

                res.data.openId && Common.setOpenId(res.data.openId);

                // context.showSuccessPage(res.data.qrcode);
                context.showSuccessPage();
            },
            error: function () {
                Common.hideLoadingPage();
                //显示错误页面
                Common.showErrorPage();
            }
        });
    },
    //显示成功页面
    showSuccessPage: function() {
        var html = '<div class="page_index">' +
            '    <div class="has-tissue">' +
            // '        <img src="'+ qrcode +'" class="qr_img">' +
            '        <div class="free-tissue-btn" onclick="Home.goToAdPage()"></div>' +
            '        <div class="btn-buy" id="btnBuy" onclick="Home.onBuyTissue()"></div>' +
            '    </div>' +
            '</div>';

        $(document.body).append($(html));
    },
    //进入广告页面
    goToAdPage: function () {
        var redirect_uri = encodeURIComponent('https://vonechn.com/weixin/ad.html');

        window.location.href="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx9d8d2cb6c5fcd0fe&redirect_uri="+ redirect_uri +"&response_type=code&scope=snsapi_base&state=vone#wechat_redirect";
    }
}

Home.init();