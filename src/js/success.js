/*
*  微信扫码出纸巾完成页面
* */
'use strict';

var Success = {
    init: function () {
        Common.addLoadingPage();

        var qid = Common.getUrlParam('qid');

        if (!qid) {
            Common.hideLoadingPage();
            Common.showErrorPage("未找到广告商公众号ID, 请退出重试或联系客服");
            return;
        }

        var redirect_uri = encodeURIComponent('https://vonechn.com/weixin/final.html?qid=' + qid);

        window.location.href="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx9d8d2cb6c5fcd0fe&redirect_uri="+ redirect_uri +"&response_type=code&scope=snsapi_base&state=vone#wechat_redirect";
    }
}

Success.init();