/*
* 公共类
* */
'use strict';

var MAX_TISSUE_COUNT = 4;
var CLS_HIDE = 'hide';
//dev
//var SeverUrl = "http://47.97.199.81";
//prod
var SeverUrl = "";

var Common = {
    code: null,
    openId: null, //用户微信id
    unitPrice: 0, //纸巾单价
    sign: null, //beecloud签名
    orderNo: null, //订单号,
    title: null, //订单title
    //购买纸巾
    onBuyTissue: function(data) {
        var ajaxUrl = "/service/wx/createTissueOrder";

        // Common.toast(JSON.stringify(data));

        if(!data) {
            Common.toast("服务器不太给力, 请退出重试或联系客服");
            return;
        }

        $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: data,
            dataType: "json",
            success: function (res) {
                Common.hideLoadingPage();

                // Common.toast(JSON.stringify(res));

                if(!res) {
                    Common.showErrorPage("服务器不太给力, 请退出重试或联系客服");
                    return;
                }

                if(res.status != 0) {
                    if(res.status == -5) {
                        //暂无纸巾
                        Common.showMachineNoTissueErrorPage();
                    } else if(res.status == -3 || res.status == -4){
                        Common.showMachineErrorPage();
                    } else {
                        Common.showErrorPage("服务器不太给力, 请退出重试或联系客服");
                    }
                    return;
                }

                // var box = res.data.box;
                //机器状态:R-运行中，E-异常，U-未知状态, S-停机
                // if(!box || (box.tissueCount <= MAX_TISSUE_COUNT) || (box.status != "R")) {
                //     Common.showMachineErrorPage();
                //     return;
                // }

                Common.openId = res.data.openId;
                Common.unitPrice = res.data.signMap.amount;
                Common.sign = res.data.signMap.sign;
                Common.orderNo = res.data.signMap.outTradeNo;
                Common.title = res.data.signMap.title;
                //调用beecloud支付
                Common.asyncPay();
            },
            error: function () {
                Common.hideLoadingPage();
                //显示错误页面
                Common.showErrorPage();
            }
        });
    },
    //beecloud支付
    bcPay: function() {
        //instant_channel: “wxmp”(微信扫码), “wx”(微信公众号支付), “wxwap“(微信移动网页), “bcwxwap“(BC微信移动网页)
        /**
         * click调用错误返回：默认行为console.log(err)
         */
        BC.err = function(data) {
            //注册错误信息接受
            Common.toast(data["ERROR"]);
        }
        /**
         * 3. 调用BC.click 接口传递参数
         */
        BC.click({
            "title": Common.title,
            "amount": Common.unitPrice,
            "out_trade_no": Common.orderNo, //唯一订单号
            "sign" : Common.sign,
            "openid" : Common.openId, //自行获取用户openid
            "instant_channel": "wx", //设置该字段后将直接调用渠道支付，不再显示渠道选择菜单
            // "return_url" : "http://payservice.beecloud.cn/spay/result.php", //支付成功后跳转的商户页面,可选，默认为http://payservice.beecloud.cn/spay/result.php
            /**
             * optional 为自定义参数对象，目前只支持基本类型的key ＝》 value, 不支持嵌套对象；
             * 回调时如果有optional则会传递给webhook地址，webhook的使用请查阅文档
             */
            "optional" : {"tissueOrderSn": Common.orderNo},
            "debug": false
        },
        {
            wxJsapiFail: function () {
                Common.toast("支付失败, 请退出重试或联系客服");
            },
            wxJsapiSuccess: function () {
                window.location.reload();
            }
        });
    },
    //beecloud支付判断
    asyncPay: function() {
        if (typeof BC == "undefined"){
            if( document.addEventListener ){
                document.addEventListener('beecloud:onready', Common.bcPay, false);
            }else if (document.attachEvent){
                document.attachEvent('beecloud:onready', Common.bcPay);
            }
        }else{
            Common.bcPay();
        }
    },
    /*
    * 添加加载中页面
    * */
    addLoadingPage: function() {
        if($('.app-loading').length <= 0) {
            var $loadingElem = $('<div class="app-loading"><img src="image/loading.gif"></div>');
            $(document.body).append($loadingElem);
        } else {
            $('.app-loading').removeClass(CLS_HIDE);
        }
    },
    /*
    * 关闭加载中页面
    * */
    hideLoadingPage: function() {
        if($('.app-loading').length <= 0) {
            return;
        }
        $('.app-loading').addClass(CLS_HIDE);
    },
    /*
    * 出纸成功页面
    * */
    showTissueSuccessPage: function() {
        $("body div").remove();

        var html = '<div class="page_success">' +
            '<div class="button-buy" id="btnBuy" onclick="Common.onBuyTissueClick()"></div>' +
            '</div>';

        $(document.body).append($(html));
    },
    //购买纸巾
    onBuyTissueClick: function() {
        var data = {
            type: 'again',
            openid: Common.getOpenId(),
            url: encodeURIComponent(location.href.split("#")[0])
        }

        Common.onBuyTissue(data);
    },
    /*
    * 显示错误页面
    * */
    showErrorPage: function(text) {
        $("body div").remove();

        text = text || "服务器不太给力, 请退出重试或联系客服";

        var html = '<div class="app-load-error">' +
            '    <img src="image/no-result.png">' +
            '    <p class="error-text">'+ text +'</p>' +
            '</div>';

        $(document.body).append($(html));
    },
    //显示机器故障页面
    showMachineErrorPage: function () {
        $("body div").remove();

        var html = '<div class="result-error">' +
            '    <div class="re-mask"></div>' +
            '    <div class="re-inner">' +
            '        <div class="re-text machine-error"></div>' +
            '    </div>' +
            '</div>';

        $(document.body).append($(html));
    },
    //显示机器故障页面
    showMachineNoTissueErrorPage: function () {
        $("body div").remove();

        var html = '<div class="result-error">' +
            '    <div class="re-mask"></div>' +
            '    <div class="re-inner">' +
            '        <div class="re-text no-machine-tissue-text"></div>' +
            '    </div>' +
            '</div>';

        $(document.body).append($(html));
    },
    // 提示
    toast: function(text) {
        if($('.toast').length <= 0) {
            var $toast = $('<span class="toast" style="display: none">'+ text +'</span>');
            $(document.body).append($toast);
        } else {
            $('.toast').html(text);
        }

        $('.toast').fadeIn();

        setTimeout(function () {
            $('.toast').fadeOut();
        }, 3*1000);
    },
    /*
    * 获取url参数
    * */
    getUrlParam: function(paraName) {
        var url = document.location.toString();
        var arrObj = url.split("?");

        if (arrObj.length > 1) {
            var arrPara = arrObj[1].split("&");
            var arr;

            for (var i = 0; i < arrPara.length; i++) {
                arr = arrPara[i].split("=");

                if (arr != null && arr[0] == paraName) {
                    return arr[1];
                }
            }
            return "";
        }
        else {
            return "";
        }
    },
    //绑定网页回退事件
    bindBackEvent: function () {
        history.pushState(null, null, document.URL);
        // 监听回退按钮
        window.addEventListener('popstate', function() {
            Common.closeWin();
        },false);
    },
    closeWin: function () {
        WeixinJSBridge && WeixinJSBridge.invoke('closeWindow',{},function(res){});
    },
    setOpenId: function (openId) {
        localStorage.setItem("openId", openId);
    },
    getOpenId: function () {
        return localStorage.getItem("openId");
    },
    clearInfo: function () {
        localStorage.removeItem('openId');
    }
}